# [uniform distribution in Continuous probability distribution](01_Continuous_probability_distribution/01_uniform_distribution.md)
```{r}
# Uniform distribution
uniformRandomNumber = runif(25000,0,16)
hist(uniformRandomNumber, col="yellow")
```
