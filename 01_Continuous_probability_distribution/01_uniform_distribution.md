# What is uniform distribution
https://en.wikipedia.org/wiki/Uniform_distribution_(continuous)

https://ko.wikipedia.org/wiki/%EC%97%B0%EC%86%8D%EA%B7%A0%EB%93%B1%EB%B6%84%ED%8F%AC

# Generate random number
```{r}
uniformRandomNumber = runif(25000,0,16)
hist(uniformRandomNumber, col="yellow")
```

![Alt text](uniformRandomNumber.png)
